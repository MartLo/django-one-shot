from django.shortcuts import render, get_object_or_404
from one.models import Hello


def show_message(request, id):
    hello = get_object_or_404(Hello, id=id)
    context = {
        "message_object": hello,
    }
    return render(request, "one/detail.html", context)


def list_message(request):
    hello = Hello.objects.all()
    context = {
        "lis_message": hello,

    }
    return render(request, "one/message.html", context)
